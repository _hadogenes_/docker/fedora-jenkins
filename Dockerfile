FROM fedora:36

ENV PUID=1000 PGID=1000

COPY --from=jenkins/inbound-agent /usr/share/jenkins/agent.jar /usr/share/jenkins/
COPY --from=jenkins/inbound-agent /usr/local/bin/jenkins-agent /usr/local/bin/

RUN set -x \
 && dnf install -y https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm \
 && dnf update -y --enablerepo="*source" \
 && (dnf info --enablerepo="*source" test || true) \
 && dnf install -y \
      sudo \
      pigz \
      pbzip2 \
      git \
      mercurial \
      wget \
      parallel \
      gawk \
      \
      createrepo \
      \
      nfs-utils \
      \
      java-latest-openjdk  \
      \
      rpm-sign \
      rpmdevtools \
      'dnf-command(builddep)' \
      'dnf-command(copr)' \
 && dnf groupinstall -y \
     "Development Tools" \
     "Development Libraries" \
 && dnf clean packages -y \
 \
 && groupadd --gid "$PGID" jenkins \
 && useradd -G wheel --gid "$PGID" --uid "$PUID" -m jenkins \
 && chmod 755 ~jenkins \
 && echo "jenkins ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers \
 && rm -f /var/log/lastlog /var/log/faillog \
 && chmod 666 /etc/passwd

COPY entrypoint.sh /entrypoint.sh
ENTRYPOINT [ "/entrypoint.sh" ]

USER root
